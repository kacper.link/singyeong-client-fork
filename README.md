# singyeong-client

A node.js client for [신경](https://github.com/queer/singyeong) written in TypeScript.

### Usage

Look in `examples` folder for example apps.

Documentation: https://kyokobot.github.io/node-singyeong-client/

### Installation

![npm](https://img.shields.io/npm/dm/singyeong-client)
```
$ npm install singyeong-client
$ yarn add singyeong-client 
```
