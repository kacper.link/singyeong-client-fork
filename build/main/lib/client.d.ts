/// <reference types="node" />
import { EventEmitter } from "events";
import { Op, QueryBuilder } from "./query";
export interface SingyeongClientOptions {
    applicationId: string;
    auth?: string;
    clientId: string;
    reconnect?: boolean;
    tags?: string[];
}
export declare class SingyeongClient extends EventEmitter {
    /**
     * Options passed to the client.
     */
    options: SingyeongClientOptions;
    /**
     * Whether or not the client is currently in restricted mode.
     */
    restricted: boolean;
    private secure;
    private reconnect;
    private connDSN?;
    private websocketUrl?;
    private socket?;
    private heartbeatInterval?;
    private metadataCache;
    private lastHeartbeat;
    private lastHeartbeatAck;
    latency: number;
    /**
     * Creates a new Singyeong client.
     * @param dsn Singyeong connection DSN.
     * @param options Options for newly created client.
     */
    constructor(dsn: string, options: SingyeongClientOptions);
    /**
     * Whether or not the connection to Singyeong is open.
     */
    get isOpen(): boolean;
    /**
     * Returns current connection DSN.
     */
    get dsn(): string;
    /**
     * Updates the DSN which will be used on next connection.
     * @param newDSN new DSN.
     */
    set dsn(newDSN: string);
    /**
     * Connects to currently specified Singyeong instance.
     * @return {@link SingyeongClient} instance if connection succeeds.
     */
    connect(): Promise<SingyeongClient>;
    /**
     * Updates a string metadata value.
     * @param key
     * @param value
     */
    updateMetaString(key: string, value: string): void;
    /**
     * Updates a version metadata value.
     * @param key
     * @param value
     */
    updateMetaVersion(key: string, value: string): void;
    /**
     * Updates an integer metadata value.
     * @param key
     * @param value
     */
    updateMetaInteger(key: string, value: number): void;
    /**
     * Updates a float metadata value.
     * @param key
     * @param value
     */
    updateMetaFloat(key: string, value: number): void;
    /**
     * Updates a list metadata value.
     * @param key
     * @param value
     */
    updateMetaList(key: string, value: any[]): void;
    /**
     * Sends a payload to all clients matching the query.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    broadcast(application: string | string[], key: string, query: QueryBuilder | {
        ops: Op[];
    }, payload: any, restricted?: boolean): void;
    /**
     * Sends a payload to a single client that matches specified query without waiting for a response.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    send(application: string | string[], key: string, query: QueryBuilder | {
        ops: Op[];
    }, payload: any, restricted?: boolean): void;
    private dispatchQuery;
    private onWSOpen;
    private onWSClose;
    private onWSMessage;
    private onWSError;
    private handlePayload;
    private updateMeta;
    private sendPayload;
}
