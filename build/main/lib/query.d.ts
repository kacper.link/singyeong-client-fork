declare type SingleParamOp = {
    $eq: string;
} | {
    $ne: string;
} | {
    $gt: string;
} | {
    $gte: string;
} | {
    $lt: string;
} | {
    $lte: string;
} | {
    $in: string;
} | {
    $nin: string;
} | {
    $contains: string;
} | {
    $ncontains: string;
};
declare type MultiParamOp = {
    $and: SingleParamOp[];
} | {
    $or: SingleParamOp[];
} | {
    $nor: SingleParamOp[];
};
export interface Op {
    [key: string]: SingleParamOp | MultiParamOp;
}
declare class MultiBuilder {
    private builder;
    private key;
    private opType;
    private mops;
    constructor(builder: QueryBuilder, opType: string, key: string);
    eq(value: string): MultiBuilder;
    ne(value: string): MultiBuilder;
    gt(value: string): MultiBuilder;
    gte(value: string): MultiBuilder;
    lt(value: string): MultiBuilder;
    lte(value: string): MultiBuilder;
    in(value: string): MultiBuilder;
    nin(value: string): MultiBuilder;
    contains(value: string): MultiBuilder;
    ncontains(value: string): MultiBuilder;
    end(): QueryBuilder;
}
export declare class QueryBuilder {
    ops: Op[];
    constructor();
    eq(key: string, value: string): QueryBuilder;
    ne(key: string, value: string): QueryBuilder;
    gt(key: string, value: string): QueryBuilder;
    gte(key: string, value: string): QueryBuilder;
    lt(key: string, value: string): QueryBuilder;
    lte(key: string, value: string): QueryBuilder;
    in(key: string, value: string): QueryBuilder;
    nin(key: string, value: string): QueryBuilder;
    contains(key: string, value: string): QueryBuilder;
    ncontains(key: string, value: string): QueryBuilder;
    and(key: string): MultiBuilder;
    or(key: string): MultiBuilder;
    nor(key: string): MultiBuilder;
}
export declare const query: () => QueryBuilder;
export {};
