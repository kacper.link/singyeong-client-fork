"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = __importDefault(require("debug"));
const events_1 = require("events");
const url_1 = require("url");
const ws_1 = __importDefault(require("ws"));
const payload_1 = require("./payload");
const defaultOptions = {
    applicationId: undefined,
    auth: undefined,
    clientId: undefined,
    reconnect: true,
    tags: undefined
};
const dbg = debug_1.default("singyeong");
class SingyeongClient extends events_1.EventEmitter {
    /**
     * Creates a new Singyeong client.
     * @param dsn Singyeong connection DSN.
     * @param options Options for newly created client.
     */
    constructor(dsn, options) {
        super();
        /**
         * Whether or not the client is currently in restricted mode.
         */
        this.restricted = false;
        this.lastHeartbeat = 0;
        this.lastHeartbeatAck = 0;
        this.latency = 0;
        this.secure = false;
        this.reconnect = false;
        this.lastHeartbeat = Date.now();
        this.dsn = dsn;
        this.metadataCache = new Map();
        this.options = Object.assign(Object.assign({}, defaultOptions), options);
    }
    /**
     * Whether or not the connection to Singyeong is open.
     */
    get isOpen() {
        return this.socket != null && this.socket.readyState === ws_1.default.OPEN;
    }
    /**
     * Returns current connection DSN.
     */
    get dsn() {
        return this.connDSN ? this.connDSN.href : "";
    }
    /**
     * Updates the DSN which will be used on next connection.
     * @param newDSN new DSN.
     */
    set dsn(newDSN) {
        const dsn = new url_1.URL(newDSN);
        if (dsn.protocol === "ssingyeong:") {
            this.secure = true;
        }
        else if (dsn.protocol !== "singyeong:") {
            throw new URIError("Unrecognized protocol in Singyeong DSN!");
        }
        this.connDSN = dsn;
    }
    /**
     * Connects to currently specified Singyeong instance.
     * @return {@link SingyeongClient} instance if connection succeeds.
     */
    connect() {
        if (this.isOpen) {
            dbg("Socket already open, closing...");
            this.socket.close();
        }
        if (!this.connDSN) {
            throw new Error("Attempted to connect without a proper Singyeong DSN!");
        }
        // TODO: add support for MessagePack and ETF (using Discord's Erlpack?)
        const encoding = "json";
        this.websocketUrl = `${this.secure ? "wss" : "ws"}://${this.connDSN.hostname}:${this.connDSN.port ||
            4000}/gateway/websocket?encoding=${encoding}`;
        this.socket = new ws_1.default(this.websocketUrl);
        dbg("Connection URL:", this.websocketUrl);
        this.socket.on("open", this.onWSOpen.bind(this));
        return new Promise((resolve, reject) => {
            this.socket.once("ready", () => resolve(this));
            this.socket.once("error", (e) => reject(e));
        });
    }
    /**
     * Updates a string metadata value.
     * @param key
     * @param value
     */
    updateMetaString(key, value) {
        this.updateMeta("string", "string", key, value);
    }
    /**
     * Updates a version metadata value.
     * @param key
     * @param value
     */
    updateMetaVersion(key, value) {
        this.updateMeta("version", "string", key, value);
    }
    /**
     * Updates an integer metadata value.
     * @param key
     * @param value
     */
    updateMetaInteger(key, value) {
        this.updateMeta("integer", "number", key, (value | 0));
    }
    /**
     * Updates a float metadata value.
     * @param key
     * @param value
     */
    updateMetaFloat(key, value) {
        this.updateMeta("float", "number", key, value);
    }
    /**
     * Updates a list metadata value.
     * @param key
     * @param value
     */
    updateMetaList(key, value) {
        this.updateMeta("list", "array", key, value);
    }
    /**
     * Sends a payload to all clients matching the query.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    broadcast(application, key, query, payload, restricted = false) {
        return this.dispatchQuery(application, key, query, "BROADCAST", payload, restricted);
    }
    /**
     * Sends a payload to a single client that matches specified query without waiting for a response.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    send(application, key, query, payload, restricted = false) {
        return this.dispatchQuery(application, key, query, "SEND", payload, restricted);
    }
    dispatchQuery(application, key, query, event, payload, restricted = false, nonce) {
        this.sendPayload(payload_1.OpCode.DISPATCH, {
            sender: this.options.clientId,
            nonce,
            target: {
                application,
                restricted,
                key,
                ops: query ? query.ops : null
            },
            payload
        }, event);
    }
    onWSOpen() {
        this.socket.on("message", this.onWSMessage.bind(this));
        this.socket.on("close", this.onWSClose.bind(this));
    }
    onWSClose(code) {
        dbg("WebSocket closed with code {}", code);
        if (this.heartbeatInterval) {
            clearInterval(this.heartbeatInterval);
            this.heartbeatInterval = undefined;
        }
        this.reconnect = true;
        this.connect().then(() => undefined);
    }
    onWSMessage(data) {
        try {
            if (typeof data !== "string") {
                this.onWSError(new Error("Received a non-text websocket frame!"));
                return;
            }
            this.handlePayload(payload_1.validatePayload(JSON.parse(data)));
        }
        catch (e) {
            this.onWSError(e);
        }
    }
    onWSError(error) {
        this.emit("error", error);
        this.socket.close(4011);
    }
    handlePayload(data) {
        var _a;
        dbg("->", data);
        switch (data.op) {
            case payload_1.OpCode.HELLO: {
                this.heartbeatInterval = setInterval(() => {
                    this.sendPayload(payload_1.OpCode.HEARTBEAT, { client_id: this.options.clientId });
                    this.lastHeartbeat = Date.now();
                }, data.d.heartbeat_interval);
                this.sendPayload(payload_1.OpCode.IDENTIFY, {
                    application_id: this.options.applicationId,
                    auth: this.options.auth,
                    client_id: this.options.clientId,
                    reconnect: this.reconnect,
                    tags: this.options.tags
                });
                break;
            }
            case payload_1.OpCode.READY: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;
                const { restricted } = data.d;
                this.restricted = restricted;
                dbg("Got READY, restricted?", restricted);
                if (this.metadataCache.size !== 0) {
                    dbg("Refreshing metadata cache...");
                    const metas = {};
                    this.metadataCache.forEach((meta, key) => (metas[key] = meta));
                    this.sendPayload(payload_1.OpCode.DISPATCH, metas, "UPDATE_METADATA");
                }
                (_a = this.socket) === null || _a === void 0 ? void 0 : _a.emit("ready", data.d);
                break;
            }
            case payload_1.OpCode.DISPATCH: {
                this.emit("payload", data.d);
                break;
            }
            case payload_1.OpCode.HEARTBEAT_ACK: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;
                break;
            }
            case payload_1.OpCode.GOODBYE: {
                this.socket.close(1001);
                break;
            }
        }
    }
    updateMeta(type, expected, key, value) {
        if (type === "list" ? !Array.isArray(value) : typeof value !== expected) {
            throw new TypeError(`Metadata value type is not a ${type}!`);
        }
        const meta = { type: type, value };
        this.metadataCache.set(key, meta);
        this.sendPayload(payload_1.OpCode.DISPATCH, { [key]: meta }, "UPDATE_METADATA");
    }
    sendPayload(op, d, t) {
        dbg("<-", op, d);
        if (this.isOpen) {
            this.socket.send(JSON.stringify({ op, d, t, ts: Date.now() }));
        }
    }
}
exports.SingyeongClient = SingyeongClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxrREFBMEI7QUFDMUIsbUNBQXNDO0FBQ3RDLDZCQUEwQjtBQUMxQiw0Q0FBMkI7QUFDM0IsdUNBQWdHO0FBV2hHLE1BQU0sY0FBYyxHQUFvQztJQUNwRCxhQUFhLEVBQUUsU0FBUztJQUN4QixJQUFJLEVBQUUsU0FBUztJQUNmLFFBQVEsRUFBRSxTQUFTO0lBQ25CLFNBQVMsRUFBRSxJQUFJO0lBQ2YsSUFBSSxFQUFFLFNBQVM7Q0FDbEIsQ0FBQztBQUVGLE1BQU0sR0FBRyxHQUFHLGVBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUUvQixNQUFhLGVBQWdCLFNBQVEscUJBQVk7SUF1QjdDOzs7O09BSUc7SUFDSCxZQUFZLEdBQVcsRUFBRSxPQUErQjtRQUNwRCxLQUFLLEVBQUUsQ0FBQztRQXZCWjs7V0FFRztRQUNJLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFVM0Isa0JBQWEsR0FBRyxDQUFDLENBQUM7UUFDbEIscUJBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLFlBQU8sR0FBRyxDQUFDLENBQUM7UUFTZixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQWUsQ0FBQztRQUU1QyxJQUFJLENBQUMsT0FBTyxtQ0FBUSxjQUFjLEdBQUssT0FBTyxDQUFFLENBQUM7SUFDckQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBVyxNQUFNO1FBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxZQUFTLENBQUMsSUFBSSxDQUFDO0lBQzVFLENBQUM7SUFFRDs7T0FFRztJQUNILElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNqRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxHQUFHLENBQUMsTUFBYztRQUN6QixNQUFNLEdBQUcsR0FBRyxJQUFJLFNBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU1QixJQUFJLEdBQUcsQ0FBQyxRQUFRLEtBQUssYUFBYSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQU0sSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLFlBQVksRUFBRTtZQUN0QyxNQUFNLElBQUksUUFBUSxDQUFDLHlDQUF5QyxDQUFDLENBQUM7U0FDakU7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztJQUN2QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksT0FBTztRQUNWLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxNQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDeEI7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsc0RBQXNELENBQUMsQ0FBQztTQUMzRTtRQUVELHVFQUF1RTtRQUN2RSxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFFeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUNqRyxJQUFJLCtCQUErQixRQUFRLEVBQUUsQ0FBQztRQUM5QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksWUFBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUUvQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRWpELE9BQU8sSUFBSSxPQUFPLENBQWtCLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxNQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsTUFBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxnQkFBZ0IsQ0FBQyxHQUFXLEVBQUUsS0FBYTtRQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksaUJBQWlCLENBQUMsR0FBVyxFQUFFLEtBQWE7UUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLGlCQUFpQixDQUFDLEdBQVcsRUFBRSxLQUFhO1FBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLGVBQWUsQ0FBQyxHQUFXLEVBQUUsS0FBYTtRQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksY0FBYyxDQUFDLEdBQVcsRUFBRSxLQUFZO1FBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxTQUFTLENBQUMsV0FBOEIsRUFBRSxHQUFXLEVBQUUsS0FBbUMsRUFDaEYsT0FBWSxFQUFFLGFBQXNCLEtBQUs7UUFDdEQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxJQUFJLENBQUMsV0FBOEIsRUFBRSxHQUFXLEVBQUUsS0FBbUMsRUFBRSxPQUFZLEVBQzlGLGFBQXNCLEtBQUs7UUFDbkMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUVPLGFBQWEsQ0FBQyxXQUE4QixFQUFFLEdBQVcsRUFBRSxLQUFtQyxFQUNoRixLQUEyQixFQUFFLE9BQVksRUFBRSxhQUFzQixLQUFLLEVBQUUsS0FBYztRQUN4RyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFNLENBQUMsUUFBUSxFQUFFO1lBQzlCLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7WUFDN0IsS0FBSztZQUNMLE1BQU0sRUFBRTtnQkFDSixXQUFXO2dCQUNYLFVBQVU7Z0JBQ1YsR0FBRztnQkFDSCxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO2FBQ2hDO1lBQ0QsT0FBTztTQUNWLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDZCxDQUFDO0lBRU8sUUFBUTtRQUNaLElBQUksQ0FBQyxNQUFPLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxNQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTyxTQUFTLENBQUMsSUFBWTtRQUMxQixHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFM0MsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEIsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7U0FDdEM7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTyxXQUFXLENBQUMsSUFBb0I7UUFDcEMsSUFBSTtZQUNBLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUMsQ0FBQztnQkFDbEUsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyx5QkFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3pEO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQztJQUVPLFNBQVMsQ0FBQyxLQUFZO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFTyxhQUFhLENBQUMsSUFBa0I7O1FBQ3BDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDaEIsUUFBUSxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ2IsS0FBSyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxXQUFXLENBQ2hDLEdBQUcsRUFBRTtvQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFNLENBQUMsU0FBUyxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztvQkFDekUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3BDLENBQUMsRUFDRCxJQUFJLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUM1QixDQUFDO2dCQUVGLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLEVBQUU7b0JBQzlCLGNBQWMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWE7b0JBQzFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQ3ZCLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7b0JBQ2hDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztvQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtpQkFDMUIsQ0FBQyxDQUFDO2dCQUNILE1BQU07YUFDVDtZQUNELEtBQUssZ0JBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUUxRCxNQUFNLEVBQUUsVUFBVSxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7Z0JBQzdCLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFFMUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSyxDQUFDLEVBQUU7b0JBQy9CLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO29CQUVwQyxNQUFNLEtBQUssR0FBZ0MsRUFBRSxDQUFDO29CQUM5QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQWMsRUFBRSxHQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2pGLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLGlCQUFpQixDQUFDLENBQUM7aUJBQy9EO2dCQUVELE1BQUEsSUFBSSxDQUFDLE1BQU0sMENBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUNuQyxNQUFNO2FBQ1Q7WUFDRCxLQUFLLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsTUFBTTthQUNUO1lBQ0QsS0FBSyxnQkFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUMxRCxNQUFNO2FBQ1Q7WUFDRCxLQUFLLGdCQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxNQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QixNQUFNO2FBQ1Q7U0FDSjtJQUNMLENBQUM7SUFFTyxVQUFVLENBQUMsSUFBMkIsRUFBRSxRQUFnQixFQUFFLEdBQVcsRUFBRSxLQUFVO1FBQ3JGLElBQUksSUFBSSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDckUsTUFBTSxJQUFJLFNBQVMsQ0FBQyxnQ0FBZ0MsSUFBSSxHQUFHLENBQUMsQ0FBQztTQUNoRTtRQUVELE1BQU0sSUFBSSxHQUFhLEVBQUUsSUFBSSxFQUFFLElBQW9CLEVBQUUsS0FBSyxFQUFFLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVPLFdBQVcsQ0FBQyxFQUFVLEVBQUUsQ0FBTSxFQUFFLENBQWE7UUFDakQsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDakIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLE1BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkU7SUFDTCxDQUFDO0NBQ0o7QUFuU0QsMENBbVNDIn0=