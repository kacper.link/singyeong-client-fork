"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OpCode;
(function (OpCode) {
    OpCode[OpCode["HELLO"] = 0] = "HELLO";
    OpCode[OpCode["IDENTIFY"] = 1] = "IDENTIFY";
    OpCode[OpCode["READY"] = 2] = "READY";
    OpCode[OpCode["INVALID"] = 3] = "INVALID";
    OpCode[OpCode["DISPATCH"] = 4] = "DISPATCH";
    OpCode[OpCode["HEARTBEAT"] = 5] = "HEARTBEAT";
    OpCode[OpCode["HEARTBEAT_ACK"] = 6] = "HEARTBEAT_ACK";
    OpCode[OpCode["GOODBYE"] = 7] = "GOODBYE";
})(OpCode = exports.OpCode || (exports.OpCode = {}));
exports.validatePayload = (data) => {
    if (typeof data.op !== "number" &&
        data.op < OpCode.HELLO &&
        data.op > OpCode.GOODBYE &&
        (data.hasOwnProperty("ts") && typeof data.ts !== "number") &&
        (data.t && typeof data.t !== "string")) {
        throw new Error("Invalid payload received!");
    }
    return data;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bG9hZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvcGF5bG9hZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLElBQVksTUFTWDtBQVRELFdBQVksTUFBTTtJQUNkLHFDQUFLLENBQUE7SUFDTCwyQ0FBUSxDQUFBO0lBQ1IscUNBQUssQ0FBQTtJQUNMLHlDQUFPLENBQUE7SUFDUCwyQ0FBUSxDQUFBO0lBQ1IsNkNBQVMsQ0FBQTtJQUNULHFEQUFhLENBQUE7SUFDYix5Q0FBTyxDQUFBO0FBQ1gsQ0FBQyxFQVRXLE1BQU0sR0FBTixjQUFNLEtBQU4sY0FBTSxRQVNqQjtBQWtDWSxRQUFBLGVBQWUsR0FBRyxDQUFDLElBQVMsRUFBZ0IsRUFBRTtJQUN2RCxJQUNJLE9BQU8sSUFBSSxDQUFDLEVBQUUsS0FBSyxRQUFRO1FBQzNCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEtBQUs7UUFDdEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsT0FBTztRQUN4QixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksT0FBTyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQztRQUMxRCxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksT0FBTyxJQUFJLENBQUMsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxFQUN4QztRQUNFLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztLQUNoRDtJQUVELE9BQU8sSUFBb0IsQ0FBQztBQUNoQyxDQUFDLENBQUMifQ==