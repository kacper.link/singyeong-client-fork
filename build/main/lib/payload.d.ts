export declare enum OpCode {
    HELLO = 0,
    IDENTIFY = 1,
    READY = 2,
    INVALID = 3,
    DISPATCH = 4,
    HEARTBEAT = 5,
    HEARTBEAT_ACK = 6,
    GOODBYE = 7
}
export declare type EventType = "UPDATE_METADATA" | "SEND" | "BROADCAST" | "QUERY_NODES";
export declare type MetadataType = "string" | "integer" | "float" | "version" | "list";
export declare type Metadata = {
    type: "string";
    value: string;
} | {
    type: "integer";
    value: number;
} | {
    type: "float";
    value: number;
} | {
    type: "version";
    value: string;
} | {
    type: "list";
    value: any[];
};
export interface Payload<T> {
    /**
     * Opcode
     */
    op: OpCode;
    /**
     * Payload data
     */
    d: T;
    /**
     * Timestamp
     */
    ts?: number;
    /**
     * Event type
     */
    t?: EventType;
}
export declare const validatePayload: (data: any) => Payload<any>;
