import debug from "debug";
import { EventEmitter } from "events";
import { URL } from "url";
import WebSocket from "ws";
import { OpCode, validatePayload } from "./payload";
const defaultOptions = {
    applicationId: undefined,
    auth: undefined,
    clientId: undefined,
    reconnect: true,
    tags: undefined
};
const dbg = debug("singyeong");
export class SingyeongClient extends EventEmitter {
    /**
     * Creates a new Singyeong client.
     * @param dsn Singyeong connection DSN.
     * @param options Options for newly created client.
     */
    constructor(dsn, options) {
        super();
        /**
         * Whether or not the client is currently in restricted mode.
         */
        this.restricted = false;
        this.lastHeartbeat = 0;
        this.lastHeartbeatAck = 0;
        this.latency = 0;
        this.secure = false;
        this.reconnect = false;
        this.lastHeartbeat = Date.now();
        this.dsn = dsn;
        this.metadataCache = new Map();
        this.options = { ...defaultOptions, ...options };
    }
    /**
     * Whether or not the connection to Singyeong is open.
     */
    get isOpen() {
        return this.socket != null && this.socket.readyState === WebSocket.OPEN;
    }
    /**
     * Returns current connection DSN.
     */
    get dsn() {
        return this.connDSN ? this.connDSN.href : "";
    }
    /**
     * Updates the DSN which will be used on next connection.
     * @param newDSN new DSN.
     */
    set dsn(newDSN) {
        const dsn = new URL(newDSN);
        if (dsn.protocol === "ssingyeong:") {
            this.secure = true;
        }
        else if (dsn.protocol !== "singyeong:") {
            throw new URIError("Unrecognized protocol in Singyeong DSN!");
        }
        this.connDSN = dsn;
    }
    /**
     * Connects to currently specified Singyeong instance.
     * @return {@link SingyeongClient} instance if connection succeeds.
     */
    connect() {
        if (this.isOpen) {
            dbg("Socket already open, closing...");
            this.socket.close();
        }
        if (!this.connDSN) {
            throw new Error("Attempted to connect without a proper Singyeong DSN!");
        }
        // TODO: add support for MessagePack and ETF (using Discord's Erlpack?)
        const encoding = "json";
        this.websocketUrl = `${this.secure ? "wss" : "ws"}://${this.connDSN.hostname}:${this.connDSN.port ||
            4000}/gateway/websocket?encoding=${encoding}`;
        this.socket = new WebSocket(this.websocketUrl);
        dbg("Connection URL:", this.websocketUrl);
        this.socket.on("open", this.onWSOpen.bind(this));
        return new Promise((resolve, reject) => {
            this.socket.once("ready", () => resolve(this));
            this.socket.once("error", (e) => reject(e));
        });
    }
    /**
     * Updates a string metadata value.
     * @param key
     * @param value
     */
    updateMetaString(key, value) {
        this.updateMeta("string", "string", key, value);
    }
    /**
     * Updates a version metadata value.
     * @param key
     * @param value
     */
    updateMetaVersion(key, value) {
        this.updateMeta("version", "string", key, value);
    }
    /**
     * Updates an integer metadata value.
     * @param key
     * @param value
     */
    updateMetaInteger(key, value) {
        this.updateMeta("integer", "number", key, (value | 0));
    }
    /**
     * Updates a float metadata value.
     * @param key
     * @param value
     */
    updateMetaFloat(key, value) {
        this.updateMeta("float", "number", key, value);
    }
    /**
     * Updates a list metadata value.
     * @param key
     * @param value
     */
    updateMetaList(key, value) {
        this.updateMeta("list", "array", key, value);
    }
    /**
     * Sends a payload to all clients matching the query.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    broadcast(application, key, query, payload, restricted = false) {
        return this.dispatchQuery(application, key, query, "BROADCAST", payload, restricted);
    }
    /**
     * Sends a payload to a single client that matches specified query without waiting for a response.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    send(application, key, query, payload, restricted = false) {
        return this.dispatchQuery(application, key, query, "SEND", payload, restricted);
    }
    dispatchQuery(application, key, query, event, payload, restricted = false, nonce) {
        this.sendPayload(OpCode.DISPATCH, {
            sender: this.options.clientId,
            nonce,
            target: {
                application,
                restricted,
                key,
                ops: query ? query.ops : null
            },
            payload
        }, event);
    }
    onWSOpen() {
        this.socket.on("message", this.onWSMessage.bind(this));
        this.socket.on("close", this.onWSClose.bind(this));
    }
    onWSClose(code) {
        dbg("WebSocket closed with code {}", code);
        if (this.heartbeatInterval) {
            clearInterval(this.heartbeatInterval);
            this.heartbeatInterval = undefined;
        }
        this.reconnect = true;
        this.connect().then(() => undefined);
    }
    onWSMessage(data) {
        try {
            if (typeof data !== "string") {
                this.onWSError(new Error("Received a non-text websocket frame!"));
                return;
            }
            this.handlePayload(validatePayload(JSON.parse(data)));
        }
        catch (e) {
            this.onWSError(e);
        }
    }
    onWSError(error) {
        this.emit("error", error);
        this.socket.close(4011);
    }
    handlePayload(data) {
        dbg("->", data);
        switch (data.op) {
            case OpCode.HELLO: {
                this.heartbeatInterval = setInterval(() => {
                    this.sendPayload(OpCode.HEARTBEAT, { client_id: this.options.clientId });
                    this.lastHeartbeat = Date.now();
                }, data.d.heartbeat_interval);
                this.sendPayload(OpCode.IDENTIFY, {
                    application_id: this.options.applicationId,
                    auth: this.options.auth,
                    client_id: this.options.clientId,
                    reconnect: this.reconnect,
                    tags: this.options.tags
                });
                break;
            }
            case OpCode.READY: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;
                const { restricted } = data.d;
                this.restricted = restricted;
                dbg("Got READY, restricted?", restricted);
                if (this.metadataCache.size !== 0) {
                    dbg("Refreshing metadata cache...");
                    const metas = {};
                    this.metadataCache.forEach((meta, key) => (metas[key] = meta));
                    this.sendPayload(OpCode.DISPATCH, metas, "UPDATE_METADATA");
                }
                this.socket?.emit("ready", data.d);
                break;
            }
            case OpCode.DISPATCH: {
                this.emit("payload", data.d);
                break;
            }
            case OpCode.HEARTBEAT_ACK: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;
                break;
            }
            case OpCode.GOODBYE: {
                this.socket.close(1001);
                break;
            }
        }
    }
    updateMeta(type, expected, key, value) {
        if (type === "list" ? !Array.isArray(value) : typeof value !== expected) {
            throw new TypeError(`Metadata value type is not a ${type}!`);
        }
        const meta = { type: type, value };
        this.metadataCache.set(key, meta);
        this.sendPayload(OpCode.DISPATCH, { [key]: meta }, "UPDATE_METADATA");
    }
    sendPayload(op, d, t) {
        dbg("<-", op, d);
        if (this.isOpen) {
            this.socket.send(JSON.stringify({ op, d, t, ts: Date.now() }));
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQzFCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDdEMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLEtBQUssQ0FBQztBQUMxQixPQUFPLFNBQVMsTUFBTSxJQUFJLENBQUM7QUFDM0IsT0FBTyxFQUFxQyxNQUFNLEVBQVcsZUFBZSxFQUFFLE1BQU0sV0FBVyxDQUFDO0FBV2hHLE1BQU0sY0FBYyxHQUFvQztJQUNwRCxhQUFhLEVBQUUsU0FBUztJQUN4QixJQUFJLEVBQUUsU0FBUztJQUNmLFFBQVEsRUFBRSxTQUFTO0lBQ25CLFNBQVMsRUFBRSxJQUFJO0lBQ2YsSUFBSSxFQUFFLFNBQVM7Q0FDbEIsQ0FBQztBQUVGLE1BQU0sR0FBRyxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUUvQixNQUFNLE9BQU8sZUFBZ0IsU0FBUSxZQUFZO0lBdUI3Qzs7OztPQUlHO0lBQ0gsWUFBWSxHQUFXLEVBQUUsT0FBK0I7UUFDcEQsS0FBSyxFQUFFLENBQUM7UUF2Qlo7O1dBRUc7UUFDSSxlQUFVLEdBQVksS0FBSyxDQUFDO1FBVTNCLGtCQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLHFCQUFnQixHQUFHLENBQUMsQ0FBQztRQUN0QixZQUFPLEdBQUcsQ0FBQyxDQUFDO1FBU2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksR0FBRyxFQUFlLENBQUM7UUFFNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLEdBQUcsY0FBYyxFQUFFLEdBQUcsT0FBTyxFQUFFLENBQUM7SUFDckQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBVyxNQUFNO1FBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsSUFBSSxDQUFDO0lBQzVFLENBQUM7SUFFRDs7T0FFRztJQUNILElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNqRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxHQUFHLENBQUMsTUFBYztRQUN6QixNQUFNLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU1QixJQUFJLEdBQUcsQ0FBQyxRQUFRLEtBQUssYUFBYSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQU0sSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLFlBQVksRUFBRTtZQUN0QyxNQUFNLElBQUksUUFBUSxDQUFDLHlDQUF5QyxDQUFDLENBQUM7U0FDakU7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztJQUN2QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksT0FBTztRQUNWLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxNQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDeEI7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsc0RBQXNELENBQUMsQ0FBQztTQUMzRTtRQUVELHVFQUF1RTtRQUN2RSxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFFeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUNqRyxJQUFJLCtCQUErQixRQUFRLEVBQUUsQ0FBQztRQUM5QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUUvQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRWpELE9BQU8sSUFBSSxPQUFPLENBQWtCLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxNQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsTUFBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxnQkFBZ0IsQ0FBQyxHQUFXLEVBQUUsS0FBYTtRQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksaUJBQWlCLENBQUMsR0FBVyxFQUFFLEtBQWE7UUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLGlCQUFpQixDQUFDLEdBQVcsRUFBRSxLQUFhO1FBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLGVBQWUsQ0FBQyxHQUFXLEVBQUUsS0FBYTtRQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksY0FBYyxDQUFDLEdBQVcsRUFBRSxLQUFZO1FBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxTQUFTLENBQUMsV0FBOEIsRUFBRSxHQUFXLEVBQUUsS0FBbUMsRUFDaEYsT0FBWSxFQUFFLGFBQXNCLEtBQUs7UUFDdEQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxJQUFJLENBQUMsV0FBOEIsRUFBRSxHQUFXLEVBQUUsS0FBbUMsRUFBRSxPQUFZLEVBQzlGLGFBQXNCLEtBQUs7UUFDbkMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUVPLGFBQWEsQ0FBQyxXQUE4QixFQUFFLEdBQVcsRUFBRSxLQUFtQyxFQUNoRixLQUEyQixFQUFFLE9BQVksRUFBRSxhQUFzQixLQUFLLEVBQUUsS0FBYztRQUN4RyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDOUIsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUTtZQUM3QixLQUFLO1lBQ0wsTUFBTSxFQUFFO2dCQUNKLFdBQVc7Z0JBQ1gsVUFBVTtnQkFDVixHQUFHO2dCQUNILEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUk7YUFDaEM7WUFDRCxPQUFPO1NBQ1YsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNkLENBQUM7SUFFTyxRQUFRO1FBQ1osSUFBSSxDQUFDLE1BQU8sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLE1BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVPLFNBQVMsQ0FBQyxJQUFZO1FBQzFCLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUUzQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixhQUFhLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQztTQUN0QztRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVPLFdBQVcsQ0FBQyxJQUFvQjtRQUNwQyxJQUFJO1lBQ0EsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRSxPQUFPO2FBQ1Y7WUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6RDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyQjtJQUNMLENBQUM7SUFFTyxTQUFTLENBQUMsS0FBWTtRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRU8sYUFBYSxDQUFDLElBQWtCO1FBQ3BDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDaEIsUUFBUSxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ2IsS0FBSyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFdBQVcsQ0FDaEMsR0FBRyxFQUFFO29CQUNELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7b0JBQ3pFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNwQyxDQUFDLEVBQ0QsSUFBSSxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FDNUIsQ0FBQztnQkFFRixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7b0JBQzlCLGNBQWMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWE7b0JBQzFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7b0JBQ3ZCLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7b0JBQ2hDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztvQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTtpQkFDMUIsQ0FBQyxDQUFDO2dCQUNILE1BQU07YUFDVDtZQUNELEtBQUssTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBRTFELE1BQU0sRUFBRSxVQUFVLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsR0FBRyxDQUFDLHdCQUF3QixFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUUxQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtvQkFDL0IsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7b0JBRXBDLE1BQU0sS0FBSyxHQUFnQyxFQUFFLENBQUM7b0JBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBYyxFQUFFLEdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDakYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2lCQUMvRDtnQkFFRCxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNO2FBQ1Q7WUFDRCxLQUFLLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixNQUFNO2FBQ1Q7WUFDRCxLQUFLLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztnQkFDMUQsTUFBTTthQUNUO1lBQ0QsS0FBSyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxNQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QixNQUFNO2FBQ1Q7U0FDSjtJQUNMLENBQUM7SUFFTyxVQUFVLENBQUMsSUFBMkIsRUFBRSxRQUFnQixFQUFFLEdBQVcsRUFBRSxLQUFVO1FBQ3JGLElBQUksSUFBSSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDckUsTUFBTSxJQUFJLFNBQVMsQ0FBQyxnQ0FBZ0MsSUFBSSxHQUFHLENBQUMsQ0FBQztTQUNoRTtRQUVELE1BQU0sSUFBSSxHQUFhLEVBQUUsSUFBSSxFQUFFLElBQW9CLEVBQUUsS0FBSyxFQUFFLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRU8sV0FBVyxDQUFDLEVBQVUsRUFBRSxDQUFNLEVBQUUsQ0FBYTtRQUNqRCxHQUFHLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsTUFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNuRTtJQUNMLENBQUM7Q0FDSiJ9