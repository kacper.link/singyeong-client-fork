import { query, SingyeongClient } from "../";
import readline from "readline";

const client = new SingyeongClient("singyeong://localhost:4000", {
    clientId: "hello-owo" + (Math.random() * 10000 | 0),
    applicationId: "kyoko"
});
client.on("error", console.error);

(async () => {
    try {
        await client.connect();
        client.updateMetaString("type", "test");
        client.updateMetaList("dupa", ["owo", "uwu"]);
        client.updateMetaList("cyc", ["uwu"]);
        client.updateMetaInteger("cyc2", 999.5);
        client.on("payload", e => {
            if (e.payload.message) {
                console.log("\r<" + e.sender + "> " + e.payload.message);
                process.stdout.write("test> ");
            }
        });

        console.log("Connected!");

        process.stdout.write("test> ");
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.on("line", (input) => {
            client.broadcast("kyoko",
                (Math.random() * 10000 | 0).toString(),
                query().eq("type", "test"),
                { message: input });
            process.stdout.write("test> ");
        });
    } catch (e) {
        console.log("Can't connect lol", e);
    }
})();
