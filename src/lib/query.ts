type SingleParamOp = { $eq: string }
    | { $ne: string }
    | { $gt: string }
    | { $gte: string }
    | { $lt: string }
    | { $lte: string }
    | { $in: string }
    | { $nin: string }
    | { $contains: string }
    | { $ncontains: string };

type MultiParamOp = { $and: SingleParamOp[]; }
    | { $or: SingleParamOp[] }
    | { $nor: SingleParamOp[] };
type MultiParamOpType = keyof MultiParamOp;

export interface Op {
    [key: string]: SingleParamOp | MultiParamOp
}

class MultiBuilder {
    private builder: QueryBuilder;
    private key: string;
    private opType: string;
    private mops: SingleParamOp[];

    constructor(builder: QueryBuilder, opType: string, key: string) {
        this.builder = builder;
        this.key = key;
        this.opType = opType;
        this.mops = [];
    }

    public eq(value: string): MultiBuilder {
        this.mops.push({ $eq: value });
        return this;
    }

    public ne(value: string): MultiBuilder {
        this.mops.push({ $ne: value });
        return this;
    }

    public gt(value: string): MultiBuilder {
        this.mops.push({ $gt: value });
        return this;
    }

    public gte(value: string): MultiBuilder {
        this.mops.push({ $gte: value });
        return this;
    }

    public lt(value: string): MultiBuilder {
        this.mops.push({ $lt: value });
        return this;
    }

    public lte(value: string): MultiBuilder {
        this.mops.push({ $lte: value });
        return this;
    }

    public in(value: string): MultiBuilder {
        this.mops.push({ $in: value });
        return this;
    }

    public nin(value: string): MultiBuilder {
        this.mops.push({ $nin: value });
        return this;
    }

    public contains(value: string): MultiBuilder {
        this.mops.push({ $contains: value });
        return this;
    }

    public ncontains(value: string): MultiBuilder {
        this.mops.push({ $ncontains: value });
        return this;
    }

    public end(): QueryBuilder {
        const retOp = { [this.key]: { [this.opType]: this.mops } };
        this.builder.ops.push(retOp as Op);
        return this.builder;
    }
}

export class QueryBuilder {
    public ops: Op[];

    constructor() {
        this.ops = [];
    }

    public eq(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $eq: value } });
        return this;
    }

    public ne(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $ne: value } });
        return this;
    }

    public gt(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $gt: value } });
        return this;
    }

    public gte(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $gte: value } });
        return this;
    }

    public lt(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $lt: value } });
        return this;
    }

    public lte(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $lte: value } });
        return this;
    }

    public in(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $in: value } });
        return this;
    }

    public nin(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $nin: value } });
        return this;
    }

    public contains(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $contains: value } });
        return this;
    }

    public ncontains(key: string, value: string): QueryBuilder {
        this.ops.push({ [key]: { $ncontains: value } });
        return this;
    }

    public and(key: string): MultiBuilder {
        return new MultiBuilder(this, "$and", key);
    }

    public or(key: string): MultiBuilder {
        return new MultiBuilder(this, "$or", key);
    }

    public nor(key: string): MultiBuilder {
        return new MultiBuilder(this, "$nor", key);
    }
}

export const query = () => new QueryBuilder();
