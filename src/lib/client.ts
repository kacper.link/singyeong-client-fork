import debug from "debug";
import { EventEmitter } from "events";
import { URL } from "url";
import WebSocket from "ws";
import { EventType, Metadata, MetadataType, OpCode, Payload, validatePayload } from "./payload";
import { Op, QueryBuilder } from "./query";

export interface SingyeongClientOptions {
    applicationId: string;
    auth?: string;
    clientId: string;
    reconnect?: boolean;
    tags?: string[];
}

const defaultOptions: Partial<SingyeongClientOptions> = {
    applicationId: undefined,
    auth: undefined,
    clientId: undefined,
    reconnect: true,
    tags: undefined
};

const dbg = debug("singyeong");

export class SingyeongClient extends EventEmitter {
    /**
     * Options passed to the client.
     */
    public options: SingyeongClientOptions;

    /**
     * Whether or not the client is currently in restricted mode.
     */
    public restricted: boolean = false;

    private secure: boolean;
    private reconnect: boolean;
    private connDSN?: URL;
    private websocketUrl?: string;
    private socket?: WebSocket;
    private heartbeatInterval?: NodeJS.Timeout;
    private metadataCache: Map<string, Metadata>;

    private lastHeartbeat = 0;
    private lastHeartbeatAck = 0;
    public latency = 0;

    /**
     * Creates a new Singyeong client.
     * @param dsn Singyeong connection DSN.
     * @param options Options for newly created client.
     */
    constructor(dsn: string, options: SingyeongClientOptions) {
        super();
        this.secure = false;
        this.reconnect = false;
        this.lastHeartbeat = Date.now();
        this.dsn = dsn;
        this.metadataCache = new Map<string, any>();

        this.options = { ...defaultOptions, ...options };
    }

    /**
     * Whether or not the connection to Singyeong is open.
     */
    public get isOpen(): boolean {
        return this.socket != null && this.socket.readyState === WebSocket.OPEN;
    }

    /**
     * Returns current connection DSN.
     */
    public get dsn(): string {
        return this.connDSN ? this.connDSN.href : "";
    }

    /**
     * Updates the DSN which will be used on next connection.
     * @param newDSN new DSN.
     */
    public set dsn(newDSN: string) {
        const dsn = new URL(newDSN);

        if (dsn.protocol === "ssingyeong:") {
            this.secure = true;
        } else if (dsn.protocol !== "singyeong:") {
            throw new URIError("Unrecognized protocol in Singyeong DSN!");
        }

        this.connDSN = dsn;
    }

    /**
     * Connects to currently specified Singyeong instance.
     * @return {@link SingyeongClient} instance if connection succeeds.
     */
    public connect(): Promise<SingyeongClient> {
        if (this.isOpen) {
            dbg("Socket already open, closing...");
            this.socket!.close();
        }

        if (!this.connDSN) {
            throw new Error("Attempted to connect without a proper Singyeong DSN!");
        }

        // TODO: add support for MessagePack and ETF (using Discord's Erlpack?)
        const encoding = "json";

        this.websocketUrl = `${this.secure ? "wss" : "ws"}://${this.connDSN.hostname}:${this.connDSN.port ||
        4000}/gateway/websocket?encoding=${encoding}`;
        this.socket = new WebSocket(this.websocketUrl);

        dbg("Connection URL:", this.websocketUrl);
        this.socket.on("open", this.onWSOpen.bind(this));

        return new Promise<SingyeongClient>((resolve, reject) => {
            this.socket!.once("ready", () => resolve(this));
            this.socket!.once("error", (e) => reject(e));
        });
    }

    /**
     * Updates a string metadata value.
     * @param key
     * @param value
     */
    public updateMetaString(key: string, value: string) {
        this.updateMeta("string", "string", key, value);
    }

    /**
     * Updates a version metadata value.
     * @param key
     * @param value
     */
    public updateMetaVersion(key: string, value: string) {
        this.updateMeta("version", "string", key, value);
    }

    /**
     * Updates an integer metadata value.
     * @param key
     * @param value
     */
    public updateMetaInteger(key: string, value: number) {
        this.updateMeta("integer", "number", key, (value | 0));
    }

    /**
     * Updates a float metadata value.
     * @param key
     * @param value
     */
    public updateMetaFloat(key: string, value: number) {
        this.updateMeta("float", "number", key, value);
    }

    /**
     * Updates a list metadata value.
     * @param key
     * @param value
     */
    public updateMetaList(key: string, value: any[]) {
        this.updateMeta("list", "array", key, value);
    }

    /**
     * Sends a payload to all clients matching the query.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    public broadcast(application: string | string[], key: string, query: QueryBuilder | { ops: Op[] },
                     payload: any, restricted: boolean = false) {
        return this.dispatchQuery(application, key, query, "BROADCAST", payload, restricted);
    }

    /**
     * Sends a payload to a single client that matches specified query without waiting for a response.
     * @param application application ID or list of tags
     * @param key the hashing key
     * @param query query operators, also see {@link QueryBuilder}
     * @param payload the data to be sent/
     * @param restricted (optional) whether or not restricted clients should be included in the query.
     */
    public send(application: string | string[], key: string, query: QueryBuilder | { ops: Op[] }, payload: any,
                restricted: boolean = false) {
        return this.dispatchQuery(application, key, query, "SEND", payload, restricted);
    }

    private dispatchQuery(application: string | string[], key: string, query: QueryBuilder | { ops: Op[] },
                          event: "BROADCAST" | "SEND", payload: any, restricted: boolean = false, nonce?: string) {
        this.sendPayload(OpCode.DISPATCH, {
            sender: this.options.clientId,
            nonce,
            target: {
                application,
                restricted,
                key,
                ops: query ? query.ops : null
            },
            payload
        }, event);
    }

    private onWSOpen() {
        this.socket!.on("message", this.onWSMessage.bind(this));
        this.socket!.on("close", this.onWSClose.bind(this));
    }

    private onWSClose(code: number) {
        dbg("WebSocket closed with code {}", code);

        if (this.heartbeatInterval) {
            clearInterval(this.heartbeatInterval);
            this.heartbeatInterval = undefined;
        }
        this.reconnect = true;
        this.connect().then(() => undefined);
    }

    private onWSMessage(data: WebSocket.Data) {
        try {
            if (typeof data !== "string") {
                this.onWSError(new Error("Received a non-text websocket frame!"));
                return;
            }

            this.handlePayload(validatePayload(JSON.parse(data)));
        } catch (e) {
            this.onWSError(e);
        }
    }

    private onWSError(error: Error) {
        this.emit("error", error);
        this.socket!.close(4011);
    }

    private handlePayload(data: Payload<any>) {
        dbg("->", data);
        switch (data.op) {
            case OpCode.HELLO: {
                this.heartbeatInterval = setInterval(
                    () => {
                        this.sendPayload(OpCode.HEARTBEAT, { client_id: this.options.clientId });
                        this.lastHeartbeat = Date.now();
                    },
                    data.d.heartbeat_interval
                );

                this.sendPayload(OpCode.IDENTIFY, {
                    application_id: this.options.applicationId,
                    auth: this.options.auth,
                    client_id: this.options.clientId,
                    reconnect: this.reconnect,
                    tags: this.options.tags
                });
                break;
            }
            case OpCode.READY: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;

                const { restricted } = data.d;
                this.restricted = restricted;
                dbg("Got READY, restricted?", restricted);

                if (this.metadataCache.size !== 0) {
                    dbg("Refreshing metadata cache...");

                    const metas: { [key: string]: Metadata } = {};
                    this.metadataCache.forEach((meta: Metadata, key: string) => (metas[key] = meta));
                    this.sendPayload(OpCode.DISPATCH, metas, "UPDATE_METADATA");
                }

                this.socket?.emit("ready", data.d);
                break;
            }
            case OpCode.DISPATCH: {
                this.emit("payload", data.d);
                break;
            }
            case OpCode.HEARTBEAT_ACK: {
                this.lastHeartbeatAck = Date.now();
                this.latency = this.lastHeartbeatAck - this.lastHeartbeat;
                break;
            }
            case OpCode.GOODBYE: {
                this.socket!.close(1001);
                break;
            }
        }
    }

    private updateMeta(type: MetadataType | "list", expected: string, key: string, value: any) {
        if (type === "list" ? !Array.isArray(value) : typeof value !== expected) {
            throw new TypeError(`Metadata value type is not a ${type}!`);
        }

        const meta: Metadata = { type: type as MetadataType, value };
        this.metadataCache.set(key, meta);
        this.sendPayload(OpCode.DISPATCH, { [key]: meta }, "UPDATE_METADATA");
    }

    private sendPayload(op: OpCode, d: any, t?: EventType) {
        dbg("<-", op, d);
        if (this.isOpen) {
            this.socket!.send(JSON.stringify({ op, d, t, ts: Date.now() }));
        }
    }
}
