export enum OpCode {
    HELLO,
    IDENTIFY,
    READY,
    INVALID,
    DISPATCH,
    HEARTBEAT,
    HEARTBEAT_ACK,
    GOODBYE
}

export type EventType = "UPDATE_METADATA" | "SEND" | "BROADCAST" | "QUERY_NODES";

export type MetadataType = "string" | "integer" | "float" | "version" | "list";
export type Metadata =
    | { type: "string"; value: string }
    | { type: "integer"; value: number }
    | { type: "float"; value: number }
    | { type: "version"; value: string }
    | { type: "list"; value: any[] };

export interface Payload<T> {
    /**
     * Opcode
     */
    op: OpCode;

    /**
     * Payload data
     */
    d: T;

    /**
     * Timestamp
     */
    ts?: number;

    /**
     * Event type
     */
    t?: EventType;
}

export const validatePayload = (data: any): Payload<any> => {
    if (
        typeof data.op !== "number" &&
        data.op < OpCode.HELLO &&
        data.op > OpCode.GOODBYE &&
        (data.hasOwnProperty("ts") && typeof data.ts !== "number") &&
        (data.t && typeof data.t !== "string")
    ) {
        throw new Error("Invalid payload received!");
    }

    return data as Payload<any>;
};
